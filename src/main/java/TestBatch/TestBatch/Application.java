package TestBatch.TestBatch;

import static org.quartz.TriggerBuilder.*;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;

import TestBatch.TestBatch.job.SendMailJob;

import static org.quartz.JobBuilder.*;

public class Application {

	public void run() throws Exception {
		System.out.println("Debut Application.run");
		SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();

  	  Scheduler sched = schedFact.getScheduler();

  	  sched.start();
  	  // define the job and tie it to our HelloJob class
  	  JobDetail job = newJob(SendMailJob.class)
  	      .withIdentity("myJob", "group1") // name "myJob", group "group1"
  	      .build();
  	

  	
	TriggerBuilder<SimpleTrigger> triggerBuilder = newTrigger().withIdentity("myTrigger", "group1").startNow().withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(10)
	          .repeatForever());
    	 
  	Trigger trigger = triggerBuilder.build();



  	  // Tell quartz to schedule the job using our trigger
  	
		sched.scheduleJob(job, trigger);
	
	}

}
